# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# Class for 'Data' tab

import pygame

import config
from pipboy_tab import PipBoyTab


class Mode_Quests:

    def __init__(self, *args, **kwargs):
        self.parent = args[0]
        self.rootParent = self.parent.rootParent
        self.name = "Quests"
        self.changed = True
        self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

    def drawPage(self):
        pageChanged = self.changed
        self.changed = False
        if (pageChanged):
            True
        return self.pageCanvas, pageChanged

    # Called every view changes to this page:
    def resetPage(self):
        True

    # Consume events passed to this page:
    def ctrlEvents(self, events):
        True


class Mode_Misc:

    def __init__(self, *args, **kwargs):
        self.parent = args[0]
        self.rootParent = self.parent.rootParent
        self.name = "Misc"
        self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))
        self.changed = True

    def drawPage(self):
        pageChanged = self.changed
        self.changed = False
        if (pageChanged):
            True
        return self.pageCanvas, pageChanged

    # Called every view changes to this page:
    def resetPage(self):
        True

    # Consume events passed to this page:
    def ctrlEvents(self, events):
        True


class Tab_Data(PipBoyTab):

    def __init__(self, parent):
        super().__init__(
            parent=parent,
            name="DATA",
            modes=lambda parent: [Mode_Quests(parent), Mode_Misc(parent)]
        )