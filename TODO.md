# TODO.md

## Pip-boy ToDo list

## STATS
--------
### Status Page
1. Randomize RAD meter and have it beep alarm if above 50%. Initial random number between 1-100 then cool off period where it either lowers slowly then begin again or goes up slowly and begins again. Include geiger crackle sound.

### Special Page
1. Order SPECIAL in correct order
2. Add animated SPECIAL attribute pictures

### Skills Page
1. Create pictures and stats

### General Page
1. Create pictures and stats
		
## ITEMS
--------
### Weapons Page
1. Create pictures and stats

### Apparel Page
1. Handle EFFECTS values properly

### Aid Page
1. Handle EFFECTS values properly

### Misc Page


### Ammo Page
1. Handle EFFECTS values properly

##DATA
------
### Quests Page
2. Create pictures and stats

### Misc Page

### Radio Page
1. Implement player functionality similar to Special page with left frame item selection right frame animated wave form
2. Autoplay of music files
3. Audio off functionality
4. Only play when in focus?
5.  Needs to be able to get seperated (Colon and Pipe) data from file in the following formats:
		Radio.data:
		Radio Station 1:NULL|filepath:sounds/radio/File1.ogg
		Free Radio:NULL|filepath:sounds/radio/File2.mp3
## OTHER

### Rotary Switch Control
1. Add control of 1-4 keys using rotary Switch

### Rotary Encoder Control
1. Add control of arrow keys using rotary encoder

### QWERT Key replacement control
1. Figure out control for QWERT keys!


## System
### System Page
1. Show CPU graph
2. Show Memory usage
3. Show Disk Space
4. Show Network information

