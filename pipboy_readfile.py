# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	JPB, 2016
# Load User values from file

import sys, os, time, math
from pprint import pprint
#import config

class GetAttributes(object):


    def __init__(self, *args):
        """
            Sets up variables needed by local functions.
            :param attribute: Name of the attribute to read. Choose one of 12 defined below.
            :param value_sep: Separation character for key value
            :param value_pair_sep: Separation character for value pairs
            :param multi_value_sep: Separation character for multi-value values like effects and skills text.
            :return: None
        """
        self.configFile = args[0]
        self.result = ''
        self.lines = ''
        self.pipeSplit = []
        self.multiDict = {}
        self.valueSep = args[1]
        self.valuePairSep  = args[2]
        self.multiValueSep = args[3]

        # Move to config
        if self.configFile == 'aid':
            self.filename = "user_configs/aid.data"
        elif self.configFile == 'ammo':
            self.filename = "user_configs/ammo.data"
        elif self.configFile == 'apparel':
            self.filename = "user_configs/apparel.data"
        elif self.configFile == 'general':
            self.filename = "user_configs/general.data"
        elif self.configFile == 'special':
            self.filename = "user_configs/special.data"
        elif self.configFile == 'misc_data':
            self.filename = "user_configs/misc_data.data"
        elif self.configFile == 'misc_items':
            self.filename = "user_configs/misc_items.data"
        elif self.configFile == 'perks':
            self.filename = "user_configs/perks.data"
        elif self.configFile == 'quests':
            self.filename = "user_configs/quests.data"
        elif self.configFile == 'radio':
            self.filename = "user_configs/radio.data"
        elif self.configFile == 'skills':
            self.filename = "user_configs/skills.data"
        elif self.configFile == 'weapons':
            self.filename = "user_configs/weapons.data"
        else:
            print("Error: Unknown config file.\n")
            sys.exit(1)

        # Read the file
        self.readfile(self.filename)

        # Process the information
        self.process_fileinfo()

    def readfile(self, filename):
        """
        Reads attribute data from file into list line by line strippiung newline character.
        :param filename: Name of the file to read.
        :return: None
        """
        # Make sure file exists
        try:
            #print(filename)
            currentFile = open(filename, 'r')
        except:
            print("Error: File not found!!!\n")
            sys.exit(1)

        # Read the file lines into array and strip off newline
        self.lines = [line.rstrip('\n') for line in currentFile]
        # If enpty line drop it
        self.lines = list(line for line in self.lines if line)
        currentFile.close()

    def process_fileinfo(self):
        """
        Reads attribute data from list into multi-dimensional dicitonary.
        :param None ### TODO: Maybe we should include name of array to work on.
        :return: None
        """

        # Split lines on PIPE and store in List
        for line in self.lines:
            #print("Process line {}".format(line))
            self.pipeSplit.append(line.split(self.valuePairSep))

        # Get length and height (lists, tuples in list)
        #print("pipeSplit now: {}".format(self.pipeSplit))
        # pprint(pipeSplit)
        pipeHeight = len(self.pipeSplit)
        pipeLength = len(self.pipeSplit[0])
        #print('PipeLength= ', pipeLength, ' PipeHeight= ', pipeHeight)

        # Loop through list and create multidmiensional dicitonary of values
        for x in range(0, pipeHeight):
            #print("Now handle " + self.pipeSplit[x][0])
            # Create a new dict that you will ad in the multiDict one for each weapon
            sub_details_dict = {}
            for y in range(0, pipeLength):
                sub_details_effects = {}
                #print("Now Sub-handle " + self.pipeSplit[x][y])
                stringItem = str(self.pipeSplit[x][y])
                key, val = stringItem.split(self.valueSep)
                numberEffects = val.count(self.multiValueSep)
                if y == 0:
                    self.multiDict[key] = sub_details_dict
                # We only process and create sub-dictionary for EFFECTS if is has multiple items
                elif key == 'EFFECTS' and numberEffects >= 1:
                    #print("Number of Effects= " + str(numberEffects))
                    for z in range (0, numberEffects):
                        sub_details_effects = val.split(self.multiValueSep)
                        sub_details_dict[key] = sub_details_effects
                        #pprint(sub_details_effects)
                    #sub_details_dict[key] = val
                else:
                    sub_details_dict[key] = val

        #pprint(self.multiDict)

    def print(self):
        """
        Prints multi-dimensional dictionary.
        :param None ### TODO: Maybe we should include name of array to work on.
        :return: None
        """
        # Print whole dictionary structure
        pprint(self.multiDict)

    def copy(self):
        """
        Copies multi-dimensional dictionary and returns it.
        :param None ### TODO: Maybe we should include name of array to work on.
        :return: Multi-dimensioanl dictionary
        """
        self.result = self.multiDict
        return self.result

    ####################################

    """
        # Print Dictionary Key/Value
        for key in multiDict:
            print(key)
            print(multiDict[key]['text'])
    """

####################################################################################################################
"""
def main():
    skills = {}
    weapons = {}
    skills = GetAttributes("skills").copy()
    weapons = GetAttributes("weapons").copy()
    special = GetAttributes("special").copy()

    #test.readfile()
    #test1.readfile()
    #test1.process_fileinfo()
    #test1.print()

    #weapons = weapons.copy()
    #skills = skills.copy()
    #special = special.copy()
    #pprint(skills)
    #print('Weapons = ', weapons)

    # Print Dictionary Key/Value
    #for key in special:
    #    print(key)
    #    print(special[key]['DAM'])

    # Print primary key, sub-keys and values in multidimensional dictionary
    for key in special:
        print('key=', key)
        for val in special[key]:
            print('val=', val)
            print(special[key][val])

    # Print only Values for Damage
    for key in weapons:
        #print('key=', key)
        for val in weapons[key]:
            #print('val=', val)
            if val == 'DAM':
                print(key, 'DAM=', weapons[key][val])



    #print(multiRows)
    # Set variable:

if __name__ == "__main__":
    main()
"""