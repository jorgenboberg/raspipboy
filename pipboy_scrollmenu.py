from guipack import setup, scrollwindow
from guipack.subrect import Subrect
import config, pygame
from config import cornerHeight, cornerPadding

def create_scrollmenu(text, amounts=None):
    window_args = {"bg_color": (0, 0, 0),
                   "font": config.FONT_LRG,
                   "text_color": config.DRAWCOLOUR,
                   "text_offset": 5,
                   "sel_border_width": 2,
                   "sel_border_color": config.DRAWCOLOUR,
                   "sel_box_color": config.SELCOLOUR,
                   "bar_rect": pygame.Rect((5, cornerPadding + cornerHeight + 4,
                                            12, config.HEIGHT - 2 * (cornerHeight + cornerPadding) - 6)),
                   "bar_button_images": [setup.GFX["button_top"].subsurface((0, 0, 12, 6)),
                                         setup.GFX["button_bottom"].subsurface((0, 0, 12, 6))],
                   'amounts':amounts
                   }


    bar_subrect = Subrect(
        (config.charWidth * 2.9, config.charHeight * 3, config.WIDTH * 0.35, config.HEIGHT * 0.75))
    return scrollwindow.ScrollWindow(bar_subrect,
                                   text,
                                   **window_args)