# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# Configuration data

# Separators used in input files
VALUE_SEP = '@'
VALUEPAIR_SEP = '|'
MULTI_VALUE_SEP = '#'

# Device options
#  (These will be automatically be set to 'False' if unavailable)
USE_INTERNET = True  # Download map/place data via internet connection
USE_GPS = False  # Use GPS module, accessed via GPSD daemon
USE_SOUND = True  # Play sounds via RasPi's current sound-source
USE_CAMERA = False  # Use RasPi camera-module as V.A.T.S
USE_SERIAL = False  # Communicate with custom serial-port controller
USE_GPIO = False # Use rotary switch/encoder

ROTARY_ENCODER_FRAMES_PER_MOVE = 0
# I assume that moving by 15 items across menu is too much (FPS is 15)
ROTARY_ENCODER_TICKS_PER_MOVE = 1
# How many rotary encoder ticks are used per menu item

QUICKLOAD = False  # If true, commandline-startup bits aren't rendered
FORCE_DOWNLOAD = False  # Don't use cached map-data, if online

# Render screen-objects at this size - smaller is faster
WIDTH = 480
HEIGHT = 320

FILL_DISPLAY = False
FULLSCREEN =  False

# Address for map's default position:
#	(used if GPS is inactive)
defaultPlace = "Sundbyberg"

### TODO: Randomize this or even make it increase over time as user gains levels
# Player data:
PLAYERNAME = 'Kaio'
PLAYERLEVEL = 33
CURRHP = 180
MAXHP = 200
CURRWGT = 50
MAXWGT = 100
CURRAP = 80
MAXAP = 92
CURRXP = 725
MAXXP = 1100
CAPS = 4200
DR = 19

FPS = 15

import pygame, os, pipboy_readfile

from pipboy_readfile import *

# Initialize the empty dictionaries
aidDIC = {}
ammoDIC = {}
apparelDIC = {}
generalDIC = {}
misc_dataDIC = {}
misc_itemsDIC = {}
perksDIC = {}
questsDIC = {}
radioDIC = {}
skillsDIC = {}
specialDIC = {}
weaponsDIC = {}

# Get all the config files and store in multidimensional arrrays
#print("Eat me: \n")
#GetAttributes("apparel").print()
aidDIC = GetAttributes("aid", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
ammoDIC = GetAttributes("ammo", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
apparelDIC = GetAttributes("apparel", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
generalDIC = GetAttributes("general", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
misc_dataDIC = GetAttributes("misc_data", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
misc_itemsDIC = GetAttributes("misc_items", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
perksDIC = GetAttributes("perks", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
questsDIC = GetAttributes("quests", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
radioDIC = GetAttributes("radio", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
skillsDIC = GetAttributes("skills", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
specialDIC = GetAttributes("special", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()
weaponsDIC = GetAttributes("weapons", VALUE_SEP, VALUEPAIR_SEP, MULTI_VALUE_SEP).copy()

RADIO_STATIONS = {
    'Search' : 'sounds/radio'
}

PINS = {
    'rot_enc_1': 4,
    'rot_enc_2': 14,
    'rot_enc_btn': 15,
    'rot_sw_1': 22,
    'rot_sw_2': 24,
    'rot_sw_3': 26,
    'rot_sw_4': 28,
    'rot_sw_5': 27,
}

RWS_TO_TAB = {
    PINS['rot_sw_1']: 0,
    PINS['rot_sw_2']: 1,
    PINS['rot_sw_3']: 2,
    PINS['rot_sw_4']: 3,
    PINS['rot_sw_5']: 4
}

# My Google-API key:
# (this is limited to only 2000 location requests a day,
#    so please don't use this key if you're making your own project!)
gKey = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'

# Teensy USB serial: symbolic link set up by creating:
#   /etc/udev/rules.d/99-usb-serial.rules
# With line:
#   SUBSYSTEM=="tty", ATTRS{manufacturer}=="Teensyduino", SYMLINK+="teensy"
SERIALPORT = '/dev/teensy'
# Pi GPIO serial:
# SERIALPORT = '/dev/ttyAMA0'

try:
    import config_private

    QUICKLOAD = config_private.QUICKLOAD
    FORCE_DOWNLOAD = config_private.FORCE_DOWNLOAD
    FILL_DISPLAY = config_private.FILL_DISPLAY
    FULLSCREEN = config_private.FULLSCREEN
    defaultPlace = config_private.defaultPlace
    PLAYERNAME = config_private.PLAYERNAME
    PLAYERLEVEL = config_private.PLAYERLEVEL
    gKey = config_private.gKey
    CURRHP = config_private.CURRHP
    MAXHP = config_private.MAXHP
    CURRWGT = config_private.CURRWGT
    MAXWGT = config_private.MAXWGT
    CURRAP = config_private.CURRAP
    MAXAP = config_private.MAXAP
    CURRXP = config_private.CURRXP
    MAXXP = config_private.MAXXP
    CAPS = config_private.CAPS
    DR = config_private.DR
    RADIO_STATIONS = config_private.RADIO_STATIONS
    PINS = config_private.PINS
    USE_GPIO = config_private.USE_GPIO
except:
    print("Could not find config_private.py, using defaults")

# Test serial-controller:
if USE_SERIAL:
    # Load libraries used by serial device, if present:
    def loadSerial():
        try:
            print("Importing Serial libraries...")
            global serial
            import serial
        except:
            # Deactivate serial-related systems if load failed:
            print("SERIAL LIBRARY NOT FOUND!")
            USE_SERIAL = False


    loadSerial()
if (USE_SERIAL):
    try:
        print(("Init serial: %s" % (SERIALPORT)))
        ser = serial.Serial(SERIALPORT, 9600)
        ser.timeout = 1

        print("  Requesting device identity...")
        ser.write("\nidentify\n")

        ident = ser.readline()
        ident = ident.strip()
        print(("    Value: %s" % (str(ident))))

        if (ident != "PIPBOY"):
            print("  Pip-Boy controls not found on serial-port!")
        # config.USE_SERIAL = False

    except:
        print("* Failed to access serial! Ignoring serial port")
        USE_SERIAL = False
print(("SERIAL: %s" % (USE_SERIAL)))

# Test camera:
if USE_CAMERA:
    # Is there a camera module connected?
    def hasCamera():
        try:
            import picamera
            camera = picamera.PiCamera()
            camera.close()
            return True
        except:
            return False


    USE_CAMERA = hasCamera()
print(("CAMERA: %s" % (USE_CAMERA)))

# Downloaded/auto-generated data will be put here:
CACHEPATH = 'cache'
if not os.path.exists(CACHEPATH):
    os.makedirs(CACHEPATH)

DRAWCOLOUR = pygame.Color(255, 255, 255)
TINTCOLOUR = pygame.Color(33, 255, 156)
SELBOXGREY = 50
SELCOLOUR = pygame.Color(50, 50, 50)

EVENTS = {
    'SONG_END': pygame.USEREVENT + 1,
    'TAB_SWITCH': pygame.USEREVENT + 2,
    'PAGE_SWITCH': pygame.USEREVENT + 3
}

print("Loading images...")
IMAGES = {
    "background": pygame.image.load('images/pipboy_back.png'),
    "scanline": pygame.image.load('images/pipboyscanlines.png'),
    "distort": pygame.image.load('images/pipboydistorteffectmap.png'),
    "statusboy": pygame.image.load('images/pipboy_statusboy.png'),
    "star": pygame.image.load('images/star.png'),
    "wave1": pygame.image.load('images/radiowaves/wave1.png'),
    "wave2": pygame.image.load('images/radiowaves/wave2.png'),
    "wave3": pygame.image.load('images/radiowaves/wave3.png'),
}
print("(done)")

# Test internet connection:
if USE_INTERNET:
    import urllib.request, urllib.error, urllib.parse


    def internet_on():
        try:
            # Can we access this Google address?
            response = urllib.request.urlopen('https://www.google.com', timeout=5)
            return True
        except urllib.error.URLError as err:
            pass
        return False


    USE_INTERNET = internet_on()
print(("INTERNET: %s" % (USE_INTERNET)))

# Test and set up sounds::
MINHUMVOL = 0.7
MAXHUMVOL = 1.0
if USE_SOUND:
    try:
        print("Loading sounds...")
        pygame.mixer.init(44100, -16, 2, 2048)

        SOUNDS = {
            "start": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_access_up.wav'),
            "end": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_access_down.wav'),
            "hum": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_hum_lp.wav'),
            "scroll": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_scroll.wav'),
            "changetab": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_tab.wav'),
            "changemode": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_mode.wav'),
            "static": pygame.mixer.Sound('sounds/radio/ui_radio_static_lp.wav'),
            "tapestart": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_holotape_start.wav'),
            "tapestop": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_holotape_stop.wav'),
            "lighton": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_light_on.wav'),
            "lightoff": pygame.mixer.Sound('sounds/pipboy/ui_pipboy_light_off.wav'),
            "beacon": pygame.mixer.Sound('sounds/radio/beacon/ui_radio_beacon_header.wav'),
            "camerastart": pygame.mixer.Sound('sounds/vats/ui_vats_enter.wav'),
            # "cameraexit":	pygame.mixer.Sound('sounds/vats/ui_vats_exit.wav'),
        }
        SOUNDS["hum"].set_volume(MINHUMVOL)
        print("(done)")
    except:
        USE_SOUND = False
print(("SOUND: %s" % (USE_SOUND)))

# Set up fonts:
pygame.font.init()
kernedFontName = 'fonts/monofonto-kerned.ttf'
monoFontName = 'fonts/monofonto.ttf'

# Scale font-sizes to chosen resolution:
FONT_SML = pygame.font.Font(kernedFontName, int(HEIGHT * (12.0 / 360)))
FONT_MED = pygame.font.Font(kernedFontName, int(HEIGHT * (16.0 / 360.0)))
FONT_LRG = pygame.font.Font(kernedFontName, int(HEIGHT * (18.0 / 360.0)))
MONOFONT = pygame.font.Font(monoFontName, int(HEIGHT * (16.0 / 360.0)))

# Find monofont's character-size:
tempImg = MONOFONT.render("X", True, DRAWCOLOUR, (0, 0, 0))
charHeight = tempImg.get_height()
charWidth = tempImg.get_width()
del tempImg

# UI Corner line parameters
cornerHeight = charHeight * 1.6
cornerPadding = 10