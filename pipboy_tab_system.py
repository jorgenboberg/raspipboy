#!/usr/bin/python

# System Tab for use with: 
# 
# RasPipBoy: A Pip-Boy 3000 implementation for Raspberry Pi
#	Neal D Corbett, 2013
# Class for 'System' tab

import logging
import pygame

import config
import pipboy_headFoot as headFoot


class Tab_System:
    name = "SYSTEM"

    class Mode_System:

        changed = True

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "System"
            self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

        def drawPage(self):
            pageChanged = self.changed
            self.changed = False
            if (pageChanged):
                True
            return self.pageCanvas, pageChanged

        # Called every view changes to this page:
        def resetPage(self):
            True

        # Consume events passed to this page:
        def ctrlEvents(self, events):
            True

    class Mode_Default:
        changed = True

        def __init__(self, *args, **kwargs):
            self.parent = args[0]
            self.rootParent = self.parent.rootParent
            self.name = "Blank"
            self.pageCanvas = pygame.Surface((config.WIDTH, config.HEIGHT))

        def drawPage(self):
            pageChanged = self.changed
            self.changed = False
            if (pageChanged):
                True
            return self.pageCanvas, pageChanged

        # Called every view changes to this page:
        def resetPage(self):
            True

        # Consume events passed to this page:
        def ctrlEvents(self, events):
            True

    # Generate text for header:
    def getHeaderText(self):
        return [self.name, "System Stats", ]

    # Trigger page-functions
    def drawPage(self, modeNum):
        if modeNum >= len(self.modes):
            logging.warning("ModeNum: %s\tMode: %s\tModes: %s\n" % (str(modeNum),
                                                                    str(self.modes[modeNum]), str(self.modes)))
            modeNum = 0
        pageCanvas, pageChanged = self.modes[modeNum].drawPage()
        return pageCanvas, pageChanged

    def resetPage(self, modeNum):
        if modeNum < len(self.modes):
            self.modes[modeNum].resetPage()

    def ctrlEvents(self, pageEvents, modeNum):
        self.modes[modeNum].ctrlEvents(pageEvents)

    # Tab init:
    def __init__(self, *args, **kwargs):
        self.parent = args[0]
        self.rootParent = self.parent.rootParent
        self.canvas = pygame.Surface((config.WIDTH, config.HEIGHT))
        self.drawnPageNum = -1

        self.modes = [self.Mode_System(self), self.Mode_Default(self)]
        self.modeNames = ["", "", ]
        print((len(self.modeNames)))
        for n in range(len(self.modeNames)):
            self.modeNames[n] = self.modes[n].name

        self.header = headFoot.Header(self)
        self.footerImgs = headFoot.genFooterImgs(self.modeNames)
